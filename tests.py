import csv
import datetime
import mailstat
from mock_datetime import mock_datetime
from msg import Message

__author__ = 'arseniy.fomchenko'

import unittest

class MailStatTest(unittest.TestCase):
    TODAY = 11209600

    @classmethod
    def setUpClass(cls):
        with open('data/before.csv') as f:
            reader = csv.DictReader(f)
            msg_list_1 = [Message(int(msg['id']),
                                      int(msg['date']),
                                      msg['from'],
                                      int(msg['unread'])) for msg in reader]

        with open('data/after.csv') as f:
            reader = csv.DictReader(f)
            msg_list_2 = [Message(int(msg['id']),
                                      int(msg['date']),
                                      msg['from'],
                                      int(msg['unread'])) for msg in reader]

        cls.stat = mailstat.MailStat(msg_list_1, msg_list_2, 30)

    def lentest(self, func, expected_len):
        with mock_datetime(datetime.datetime.utcfromtimestamp(self.TODAY), datetime):
            self.assertEquals(len(func()), expected_len)

    def test_count_recieved_last_month(self):
        with mock_datetime(datetime.datetime.utcfromtimestamp(self.TODAY), datetime):
            self.assertEquals(len(self.stat.received), 35)

    def test_read_last_month(self):
        self.lentest(self.stat.received_and_read_last_month, 19)

    def test_unread_last_month(self):
        self.lentest(self.stat.received_and_unread_last_month, 16)

    def test_deleted_last_month(self):
        self.lentest(self.stat.received_and_deleted_last_month, 3)

    def test_unread_deleted_last_month(self):
        self.lentest(self.stat.unread_deleted_last_month, 2)

    def test_read_deleted_last_month(self):
        self.lentest(self.stat.read_deleted_last_month, 1)

    def test_sender_group(self):
        with mock_datetime(datetime.datetime.utcfromtimestamp(self.TODAY), datetime):
            self.assertEquals(len(self.stat.by_sender['spam']), 12)

    def test_sender_group_total(self):
        with mock_datetime(datetime.datetime.utcfromtimestamp(self.TODAY), datetime):
            self.assertEquals(len(self.stat.by_sender_total['spam']), 27)
