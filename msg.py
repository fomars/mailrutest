__author__ = 'arseniy.fomchenko'


class Message(object):

    def __init__(self, _id, date, _from, unread):
        self._id = _id
        self.date = date
        self._from = _from
        self.unread = bool(unread)

    def __eq__(self, other):
        return self._id == other._id

    def __ne__(self, other):
        return self._id != other._id
