# coding=utf-8
import csv
import datetime
from mock_datetime import mock_datetime
from msg import Message

__author__ = 'arseniy.fomchenko'


def received(list1, list2, list1_age, days=0):
    threshold = int((datetime.datetime.utcnow() - datetime.timedelta(days=days)).strftime('%s')) if days else 0
    lists_time_border = int((datetime.datetime.utcnow() - datetime.timedelta(days=list1_age)).strftime('%s'))
    from_list_1 = filter(lambda msg: msg.date >= threshold, list1)
    from_list_2 = filter(lambda msg: msg.date > max(lists_time_border, threshold), list2)
    return from_list_1 + from_list_2


def group(msg_lst, key):
    groupped = {}
    for msg in msg_lst:
        try:
            groupped[msg.__getattribute__(key)].append(msg)
        except KeyError:
            groupped[msg.__getattribute__(key)] = [msg]
    return groupped


class MailStat(object):

    def __init__(self, list1, list2, period=30, list1_age=14):
        self.list1 = list1
        self.list2 = list2
        self.period = period
        self.list1_age = list1_age
        self._received = None
        self._received_total = None
        self._deleted = None
        self._by_sender = None
        self._by_sender_total = None

    @property
    def received(self):
        """messages recieved whithin the period"""
        if self._received is None:
            self._received = received(self.list1, self.list2, self.list1_age, self.period)
        return self._received

    @property
    def received_total(self):
        if self._received_total is None:
            self._received_total = received(self.list1, self.list2, self.list1_age)
        return self._received_total

    @property
    def deleted(self):
        """messages that were recieved and deleted whithin the period"""
        if self._deleted is None:
            deleted_ids = set([msg._id for msg in self.received]) - set([msg._id for msg in self.list2])
            self._deleted = filter(lambda msg: msg._id in deleted_ids, self.list1)
        return self._deleted

    @property
    def by_sender(self):
        if self._by_sender is None:
            self._by_sender = group(self.received, '_from')
        return self._by_sender

    @property
    def by_sender_total(self):
        if self._by_sender_total is None:
            self._by_sender_total = group(self.received_total, '_from')
        return self._by_sender_total

    def received_and_read_last_month(self):
        return filter(lambda msg: msg.unread is False, self.received)

    def received_and_unread_last_month(self):
        return filter(lambda msg: msg.unread is True, self.received)

    def received_and_deleted_last_month(self):
        return self.deleted

    def unread_deleted_last_month(self):
        # this actually counts messages which were not read untill 2 weeks ago and were deleted at some point after
        # (but they still might have been read within last 2 weeks and deleted subsequently, and we know nothing about it)
        # for the proper implementation more information needed
        return filter(lambda msg: msg.unread is True, self.deleted)

    def read_deleted_last_month(self):
        return filter(lambda msg: msg.unread is False, self.deleted)

    def print_stat(self):
        print('получено и прочитано за последний месяц: {}'.format(self.received_and_read_last_month().__len__()))
        print('получено и не прочитано за последний месяц: {}'.format(self.received_and_unread_last_month().__len__()))
        print('получено и удалено непрочитанными за последний месяц: {}'.format(self.unread_deleted_last_month().__len__()))
        print('получено и удалено прочитанными за последний месяц: {}'.format(self.read_deleted_last_month().__len__()))

        for sender, msgs in self.by_sender_total.iteritems():
            print('получено от {}: {} писем'.format(sender, len(msgs)))

        for sender, msgs in self.by_sender.iteritems():
            print('получено за последний месяц от {}: {} писем'.format(sender, len(msgs)))


if __name__ == '__main__':
    TODAY = 11209600
    source1 = 'data/before.csv'
    source2 = 'data/after.csv'
    print('loading data from {} and {}\n'.format(source1, source2))

    with open(source1) as f:
            reader = csv.DictReader(f)
            msg_list_1 = [Message(int(msg['id']),
                                      int(msg['date']),
                                      msg['from'],
                                      int(msg['unread'])) for msg in reader]

    with open(source2) as f:
        reader = csv.DictReader(f)
        msg_list_2 = [Message(int(msg['id']),
                                  int(msg['date']),
                                  msg['from'],
                                  int(msg['unread'])) for msg in reader]

    with mock_datetime(datetime.datetime.utcfromtimestamp(TODAY), datetime):
        stat = MailStat(msg_list_1, msg_list_2)
        stat.print_stat()
